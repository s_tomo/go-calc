package main

import (
	"bufio"
	"os"
	"fmt"
	"io"
	"gitlab.com/s_tomo/go-calc/calc"
)

func main() {
	var err error
	c := calc.New()
	reader := bufio.NewReaderSize(os.Stdin, 4096)
	for line := ""; err == nil; line, err = reader.ReadString('\n') {
		c.SetNewLine(line)
		c.Statement()
		fmt.Print("input>")
	}
	if err != io.EOF {
		panic(err)
	} else {
		fmt.Println("\nbyebye!")
		os.Exit(0)
	}
}
