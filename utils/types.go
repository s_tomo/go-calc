package utils

type Kind int

const (
	Print Kind = iota + 1
	Lparen
	Rparen
	Plus
	Minus
	Multi
	Divi
	Assign
	VarName
	IntNum
	EOF
	Others
)
