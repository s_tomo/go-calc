package utils

import (
	"strconv"
	"strings"
)

func IsDigit(r rune) bool {
	if _, err := strconv.Atoi(string(r)); err == nil {
		return true
	}
	return false
}

func ToLower(r rune) (rune, bool) {
	if 'a' <= r && r <= 'z' {
		return r, true
	} else if 'A' <= r && r <= 'Z' {
		return []rune(strings.ToLower(string(r)))[0], false
	} else {
		return '0', false
	}
}
