package calc

import (
	"fmt"
	"strconv"

	"gitlab.com/s_tomo/go-calc/token"
	"gitlab.com/s_tomo/go-calc/utils"
)

type calc struct {
	tokens    []*token.Token
	variables map[string]int
	stack     []int
	generator token.Genetator
}

type Calculator interface {
	SetNewLine(line string)
	Statement()
}

func New() Calculator {
	c := new(calc)
	c.tokens = make([]*token.Token, 0)
	c.variables = make(map[string]int)
	c.stack = make([]int, 0, 10)
	c.generator = nil
	return c
}

func (c *calc) SetNewLine(line string) {
	c.generator = token.NewGenerator(line)
}

func (c *calc) push(val int) {
	c.stack = append(c.stack, val)
}

func (c *calc) pop() (int, bool) {
	length := len(c.stack)
	if length > 0 {
		val := c.stack[length-1]
		c.stack = c.stack[:length-1]
		return val, true
	}
	return 0, false
}

func (c *calc) Statement() {
	if tk, ok := c.generator.ReadNext(); ok {
		switch tk.Kind {
		case utils.VarName:
			c.generator.Next()
			varName := tk.Value
			if tk, ok := c.generator.Next(); ok && tk.Kind == utils.Assign {
				c.expression()
				if c.variables[varName], ok = c.pop(); !ok {
					fmt.Printf("variables not found:%s\n", varName)
				}
			} else {
				fmt.Println("token should be '='")
			}
		case utils.Print:
			c.generator.Next()
			c.expression()
			if val, ok := c.pop(); ok {
				fmt.Printf("Answer: %d\n", val)
			}
		default:
			c.expression()
		}
	}
}

func (c *calc) expression() {
	c.term()
	for tk, ok := c.generator.ReadNext(); ok && (tk.Kind == utils.Plus || tk.Kind == utils.Minus); tk, ok = c.generator.ReadNext() {
		c.generator.Next()
		op := tk.Kind
		c.term()
		c.operate(op)
	}
}

func (c *calc) term() {
	c.factor()

	for tk, ok := c.generator.ReadNext(); ok && (tk.Kind == utils.Multi || tk.Kind == utils.Divi); tk, ok = c.generator.ReadNext() {
		c.generator.Next()
		op := tk.Kind
		c.factor()
		c.operate(op)
	}
}

func (c *calc) factor() {
	if tk, ok := c.generator.Next(); ok {
		switch tk.Kind {
		case utils.VarName:
			if val, ok := c.variables[tk.Value]; ok {
				c.push(val)
			} else {
				fmt.Printf("variables not found:%s\n", tk.Value)
			}
		case utils.IntNum:
			if num, err := strconv.Atoi(tk.Value); err == nil {
				c.push(num)
			} else {
				fmt.Printf("value cannot convert to int:%s\n", tk.Value)
			}
		case utils.Lparen:
			c.expression()
			if tk, ok := c.generator.Next(); tk.Kind != utils.Rparen && ok {
				fmt.Println("Syntax Error: missing ')' ")
			}
		}
	} else {
		fmt.Println("missing token")
	}

}

func (c *calc) operate(op utils.Kind) bool {
	var d1, d2 int
	var ok bool
	if d2, ok = c.pop(); ok {
	} else {
		fmt.Println("Invalid argument")
		return false
	}

	if d1, ok = c.pop(); ok {
	} else {
		fmt.Println("Invalid argument")
		return false
	}

	if op == utils.Divi && d2 == 0 {
		fmt.Println("Zero Divide Error")
		return false
	}

	switch op {
	case utils.Plus:
		c.push(d1 + d2)
	case utils.Minus:
		c.push(d1 - d2)
	case utils.Multi:
		c.push(d1 * d2)
	case utils.Divi:
		c.push(d1 / d2)
	default:
		fmt.Println("Invalid operator")
		return false
	}
	return true
}


