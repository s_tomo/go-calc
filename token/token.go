package token

import "gitlab.com/s_tomo/go-calc/utils"

type Token struct {
	Kind  utils.Kind
	Value string
}

type tokenGenerator struct {
	source       []rune
	maxIndex     int
	currentIndex int
}

type Genetator interface {
	Next() (*Token, bool)
	ReadNext() (*Token, bool)
	ReadFirstToken() (*Token, bool)
}

func NewGenerator(text string) Genetator {
	tg := new(tokenGenerator)
	tg.source = []rune(text)
	tg.maxIndex = len(text)
	tg.currentIndex = 0
	return tg
}

func (tg *tokenGenerator) tokenGenerator(index int) (*Token, bool, int) {
	//skip spaces
	for index < tg.maxIndex && tg.source[index] == ' ' {
		index++
	}
	if index >= tg.maxIndex {
		return nil, false, index
	}
	var token = &Token{Kind: utils.Others, Value: ""}
	ch := tg.source[index]
	if utils.IsDigit(ch) {
		token.Kind = utils.IntNum
		for utils.IsDigit(ch) {
			token.Value = token.Value + string(ch)
			index++
			if index >= tg.maxIndex {
				break
			} else {
				ch = tg.source[index]
			}
		}
	} else if num, ok := utils.ToLower(ch); ok {
		token.Value = string(num)
		token.Kind = utils.VarName
		index++
	} else {
		switch ch {
		case '(':
			token.Kind = utils.Lparen
		case ')':
			token.Kind = utils.Rparen
		case '+':
			token.Kind = utils.Plus
		case '-':
			token.Kind = utils.Minus
		case '*':
			token.Kind = utils.Multi
		case '/':
			token.Kind = utils.Divi
		case '=':
			token.Kind = utils.Assign
		case '?':
			token.Kind = utils.Print
		}
		index++
	}
	return token, true, index
}

func (tg *tokenGenerator) Next() (*Token, bool) {
	tk, ok, index := tg.tokenGenerator(tg.currentIndex)
	if ok {
		tg.currentIndex = index
	}
	return tk, ok
}

func (tg *tokenGenerator) ReadNext() (*Token, bool) {
	tk, ok, _ := tg.tokenGenerator(tg.currentIndex)
	return tk, ok
}

func (tg *tokenGenerator) ReadFirstToken() (*Token, bool) {
	tk, ok, _ := tg.tokenGenerator(0)
	return tk, ok
}
